----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/29/2020 03:25:14 PM
-- Design Name: 
-- Module Name: RAM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RAM is port(
Clk : in std_logic;
Adr : in std_logic_vector( 17 downto 0);
Data : in std_logic_vector(15 downto 0);
WE : in std_logic;
Q : out std_logic_vector( 15 downto 0));

end RAM;

architecture Behavioral of RAM is

type memorie is array( 0 to 255999) of std_logic_vector(15 downto 0);
signal mem : memorie;

begin

process(Clk)
begin
    if rising_edge(Clk) then
    Q<=mem(conv_integer(Adr));
        if WE='1' then 
            mem(conv_integer(Adr))<=Data;
        end if;
    end if;        
end process;



end Behavioral;
