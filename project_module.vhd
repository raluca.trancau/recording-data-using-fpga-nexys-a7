library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity modul_principal_P44 is
  Port (btn_u : in  STD_LOGIC;
        clk : in  STD_LOGIC;
        rst : in STD_LOGIC;
        uart_txd : out  STD_LOGIC;
        pmodbt_rst : out  STD_LOGIC;
        pmodbt_cts : out  STD_LOGIC;
        mic_clk : out std_logic;
        mic_lrsel : out std_logic;
        mic_data : in  std_logic
       );
end modul_principal_P44;

architecture Behavioral of modul_principal_P44 is


signal en_des : STD_LOGIC := '0';
signal done_des : STD_LOGIC := '0';
signal data_des : STD_LOGIC_VECTOR(15 downto 0) := "0000000000000000";
signal btnu_int : std_logic;
constant SECONDS_TO_RECORD : integer := 4; 
constant PDM_FREQ_HZ : integer := 1024000; 
constant CLK_FREQ_MHZ : integer := 100;
constant NR_OF_BITS : integer := 16;
constant NR_SAMPLES : integer := (((SECONDS_TO_RECORD*PDM_FREQ_HZ)/NR_OF_BITS) - 1);
type state is (Idle, Rec, Inter, Send);
signal st : state;
signal next_st : state;
signal addr : STD_LOGIC_VECTOR(17 downto 0) := (others => '0'); 
signal WE : STD_LOGIC := '0';
signal data_mem : STD_LOGIC_VECTOR(15 downto 0) := (others => '0'); 
signal cntRecSamples : INTEGER := 0;
signal done_des_dly : STD_LOGIC := '0';
signal send_uart_tx : STD_LOGIC := '0';
signal ready_flag : STD_LOGIC := '0';
signal done_ser : STD_LOGIC := '0';
signal done_ser_dly : STD_LOGIC := '0';
signal cntSentSamples : INTEGER := 0;
signal addr_rec : STD_LOGIC_VECTOR(17 downto 0) := (others => '0'); 
signal addr_send : STD_LOGIC_VECTOR(17 downto 0) := (others => '0');
signal half : STD_LOGIC := '0';
signal data_half_to_send : STD_LOGIC_VECTOR(7 downto 0) := (others => '0');



begin



------------------------------------------------------------------------
-- Debouncer
------------------------------------------------------------------------
    debouncer_start_record : entity WORK.debounce 
          -- generic map(
            --  NR_OF_CLKS           => 4095)
           port map(
              Clk  => clk,
              Rst  => rst,             
              Din  => btn_u,
              Qout => btnu_int);
------------------------------------------------------------------------
-- Deserializer
------------------------------------------------------------------------
    pdm_des : entity WORK.deserializator
           generic map(
              NR_OF_BITS => NR_OF_BITS,
              PDM_FREQ_HZ => PDM_FREQ_HZ,
              CLK_FREQ_MHZ => CLK_FREQ_MHZ)
           port map(
              clk => clk,
              en => en_des,
              done => done_des,
              data => data_des,
              mic_clk => mic_clk,
              mic_data => mic_data,
              mic_lrsel => mic_lrsel);
			  

		
		
		process(clk)
		begin
		if rising_edge(clk) then
			 if st = Rec then
				if done_des = '1' then
					cntRecSamples <= cntRecSamples + 1;
				end if;
				if done_des_dly = '1' then
					addr_rec <= addr_rec + 1;
				end if;
			else
				cntRecSamples <= 0;
				addr_rec <= (others => '0');
			end if;
			
			if st = Send then
            if done_ser = '1' then
                if half = '0' then
                    half <= '1';
                else 
                    half<='0';
                end if;    
               --half <= not half;
            end if;
            if done_ser_dly = '1' then
               if half='0' then
                   addr_send <= addr_send + 1;
                   cntSentSamples <= cntSentSamples + 1; 
               end if;
            end if;
         else
            cntSentSamples <= 0;
            addr_send <= (others => '0');
         end if;
		end if;	
		end process;
		
	process(clk)
	begin
		if rising_edge(clk) then
			done_des_dly <= done_des;
			done_ser_dly <= done_ser;

		end if;
	end process;	
	
------------------------------------------------------------------------
-- Memory
------------------------------------------------------------------------
    ram_memory : entity WORK.RAM
            port map (
                clk      => clk,
                Adr     => addr,
                data     => data_des,
                WE => we,
                Q   => data_mem);
------------------------------------------------------------------------
--  FSM
------------------------------------------------------------------------

	NEXT_STATE_DECODE: process(clk)
		begin 
			if rising_edge(clk) then
				if rst = '1' then
					st<=Idle;
				else
					case st is
						when Idle =>
							if btnu_int = '1' then
								st <= Rec;
							end if;
							
						when Rec => 
							if cntRecSamples = NR_SAMPLES then
								st <= Inter;
							end if;	

                        when Inter => 
							st<=Send;

						when Send =>
							if btnu_int = '1' then
								st <= Idle;
							elsif cntSentSamples = NR_SAMPLES then 
								st <= Idle;
							end if;
						
						when others =>
							st<=Idle;
					end case;
				end if;
			end if;	
		end process;		

    send_uart_tx<= ready_flag when (st=Send and cntSentSamples /= NR_SAMPLES) else '0';
	en_des<= '1' when st=Rec else '0' when (st=Idle or st=Inter);
	addr<=  addr_rec when st=Rec else addr_send when st=Send else (others => '0');
	we<= '1' when st=Rec else '0';
	data_half_to_send<= data_mem(15 downto 8) when (st=Send and half='0') else data_mem(7 downto 0) when (st=Send and half/='0');
	
	
------------------------------------------------------------------------
--  UART_TX_CTRL
------------------------------------------------------------------------   
   uart : entity WORK.uart_tx 
        port map (
            Rst => rst,
            Start    => send_uart_tx,
            TxData    => data_half_to_send,   
            CLK     => clk,
            TxRdy   => ready_flag,
            TX => uart_txd,
            DONE    => done_ser);



   pmodbt_rst <= '1';
   pmodbt_cts <= '0';




end Behavioral;





