----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 29.11.2020 16:20:11
-- Design Name: 
-- Module Name: deserializator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity deserializator is
   generic(
      NR_OF_BITS : integer := 16;
      PDM_FREQ_HZ : integer := 1_024_000;
      CLK_FREQ_MHZ : integer := 100
      
   );
   port(
      clk : in std_logic;
      en : in std_logic;
      data : out std_logic_vector(NR_OF_BITS - 1 downto 0);
      done : out std_logic;
      mic_clk : out std_logic;
      mic_data : in std_logic;
      mic_lrsel : out std_logic
   );
end deserializator;

architecture Behavioral of deserializator is

signal counter_clk : integer range 0 to 127 := 0;
signal clk_int : std_logic := '0';
signal pdm_clk_rising : std_logic;
signal pdm_shift_reg : std_logic_vector((NR_OF_BITS - 1) downto 0);
signal nr_bits : integer range 0 to 31 := 0;
signal en_int : std_logic;
signal done_int : std_logic;

begin

   mic_lrsel <= '0';
   
   p1: process(clk)
   begin
      if rising_edge(clk) then
         en_int <= en;
      end if;
   end process p1;

   p2: process(clk) 
   begin 
      if rising_edge(clk) then
         if pdm_clk_rising = '1' then 
            pdm_shift_reg((NR_OF_BITS - 1) downto 1) <= pdm_shift_reg(NR_OF_BITS-2 downto 0);
            pdm_shift_reg(0) <= mic_data;
         end if; 
      end if;
   end process p2;
   
   p3: process(clk) 
   begin
      if rising_edge(clk) then
         if en_int = '0' then
            nr_bits <= 0;
         else
            if pdm_clk_rising = '1' then
               if nr_bits = (NR_OF_BITS-1) then
                  nr_bits <= 0;
               else
                  nr_bits <= nr_bits + 1;
               end if;
            end if;
         end if;
      end if;
   end process p3;

  p4: process(clk) 
  begin
      if rising_edge(clk) then
         if pdm_clk_rising = '1' then
            if nr_bits = 0 then
               if en_int = '1' then
                  done_int <= '1';
                  data <= pdm_shift_reg;
               end if;
            end if;
         else
            done_int <= '0';
         end if;
      end if;
   end process p4;
   
   done <= done_int;

   p5: process(clk)
   begin
      if rising_edge(clk) then
            if counter_clk = (((CLK_FREQ_MHZ*1000000)/(PDM_FREQ_HZ*2))-1) then
               counter_clk <= 0;
               if clk_int = '0' then
                    clk_int <= '1';
               else
                    clk_int <= '0';
               end if;
               
               if clk_int = '0' then
                  pdm_clk_rising <= '1';
               end if;
            else
               counter_clk <= counter_clk + 1;
               pdm_clk_rising <= '0';
            end if;
      end if;
   end process p5;
   
   mic_clk <= clk_int;

end Behavioral;
